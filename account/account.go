package account

import (
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
	"log"
)

// SignedTransaction returns transaction options, signed with private key
// For now it is assumed that there is just one account, which is hardcoded below
func SignedTransaction() *bind.TransactOpts {
	// Obviously you don't want this in your repo...
	privateKey, err := crypto.HexToECDSA("0ec19102f13bccc3793b8d8e70a984a60646631fd846e461e0afbb1ff62dbab9")
	if err != nil {
		log.Fatalln("Invalid private key")
	}
	signedTx := bind.NewKeyedTransactor(privateKey)
	signedTx.From = common.HexToAddress("0x53707B530a8Af03D0889A04de063B78C4483C3f0") // pub key
	return signedTx
}
