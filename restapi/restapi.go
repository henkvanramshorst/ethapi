package main

import (
	"encoding/json"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/gorilla/mux"
	"gitlab.com/henkvanramshorst/ethapi/client"
	"log"
	"net/http"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/ethapi", PostRequest).Methods("POST")
	fmt.Println("Serving on http://localhost:8000")
	log.Fatal(http.ListenAndServe(":8000", router))
}

type incomingRequest struct {
	Fcn  string   `json:"fcn"`
	Args []string `json:"params"`
}

// PostRequest handles incoming request, calls function based on
// fcn field, and sends response
func PostRequest(w http.ResponseWriter, r *http.Request) {
	var req incomingRequest
	if err := json.NewDecoder(r.Body).Decode(&req); err != nil {
		fmt.Printf("Failed to decode request: %s\n", err)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	client := client.NewClient()

	switch req.Fcn {
	case "transfer_asset":
		switch req.Args[0] {
		case "HENK":
			trx, err := client.TransferHenkAsset(common.HexToAddress(req.Args[1]))
			if err != nil {
				w.WriteHeader(http.StatusBadRequest)
				return
			}
			fmt.Printf("Transaction successful: %v\n", trx.Hash().Hex())
		}
	}
}
