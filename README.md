# Run application
1. git clone https://gitlab.com/henkvanramshorst/ethapi.git
2. run ```go install``` in the directories account, client, contract, restapi
3. run Geth: ```geth --testnet --rpc --syncmode "light"```
4. run restapi (serving on localhost:8000)
5. make transaction ```curl -i -X POST -H "Content-Type: application/json" -d '{"fcn":"transfer_asset","params":["HENK","0x0b06b375d4b7eff730227b974d63f15dcc1df2e2"]}' http://localhost:8000/ethapi```
6. check transaction on https://ropsten.etherscan.io/address/0x53707B530a8Af03D0889A04de063B78C4483C3f0#tokentxns

# Additional information
- The asset to be transfered is the HENK ERC20 token
- Account holding most of the tokens: 0x53707B530a8Af03D0889A04de063B78C4483C3f0
