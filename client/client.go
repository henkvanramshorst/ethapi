package client

import (
	"errors"
	"fmt"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/ethclient"
	"gitlab.com/henkvanramshorst/ethapi/account"
	"gitlab.com/henkvanramshorst/ethapi/contract"
	"log"
	"math/big"
)

// API for Ethereum client
type API struct {
	client ethclient.Client
}

// NewClient sets the API url
func NewClient() API {
	// Better to use env vars instead of hardcoded default
	client, err := ethclient.Dial("http://localhost:8545")
	if err != nil {
		log.Fatalf("Failed to set up Ethereum client: %v\n", err)
	}
	return API{*client}
}

// TransferAsset transfers tokens to receiver
func (api *API) TransferHenkAsset(receiver common.Address) (*types.Transaction, error) {
	// This needs refactoring. There should be a more dynamic way to fetch the right contract,
	// based on assetID.

	// Get signed transaction with from address
	trxOptions := account.SignedTransaction()

	// Set some random amount, should be extended with a parameter from
	// the api
	amount := big.NewInt(1000)

	// Get the contract
	assetContract, err := contract.NewHenksToken(common.HexToAddress("0x9d50304Fc41748EE12f3b3600251E5e2dcd41524"), &api.client)
	if err != nil {
		return nil, errors.New("Problems calling the contract")
	}

	// Transfer assets
	trx, err := assetContract.Transfer(trxOptions, receiver, amount)
	if err != nil {
		fmt.Printf("Transaction failed: %v\n", err)
		return nil, err
	}
	return trx, nil
}
